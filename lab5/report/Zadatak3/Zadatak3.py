from cv2 import cv2 
  

camera = cv2.VideoCapture(0) 
  
while(True):
     
    ret, frame = camera.read() 
   
    cv2.imshow('frame', frame) 
 
    k = cv2.waitKey(30) & 0xff
    
    if k == 27:
        break

camera.release() 

cv2.destroyAllWindows()
