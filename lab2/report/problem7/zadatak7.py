import cv2
import numpy as np

image = cv2.imread('../../slike/baboon.bmp', 0)
image = cv2.resize(image, None, fx=0.25, fy=0.25, interpolation = cv2.INTER_CUBIC)
rows, cols = image.shape
images = []

for angle in range(0, 360, 30):
    M = cv2.getRotationMatrix2D((cols/2, rows/2), angle, 1)
    dst = cv2.warpAffine(image, M, (cols, rows))
    images.append(dst)

result = np.hstack(images)
cv2.imwrite('baboon_rotated.bmp', result)
