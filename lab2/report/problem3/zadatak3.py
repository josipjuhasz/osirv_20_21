import os
import cv2

src_dir_path='lab2/slike'
dst_dir_path='lab2/problem3'
os.makedirs(dst_dir_path, exist_ok=True)

for image_name in os.listdir(src_dir_path):
    image_path=os.path.join(src_dir_path,image_name)
    image=cv2.imread(image_path, cv2.IMREAD_GRAYSCALE)
    image_inverted=cv2.bitwise_not(image)
    image_inverted_dst_path=os.path.join(dst_dir_path,image_name.split('.')[0]+"_invert.jpg")
    print(image_inverted_dst_path)
    cv2.imwrite(image_inverted_dst_path,image_inverted)
