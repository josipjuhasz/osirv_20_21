import cv2
import os
import numpy as np

image_path='lab2/slike/BoatsColor.bmp'
image=cv2.imread(image_path,cv2.IMREAD_GRAYSCALE)

print(np.max(image), np.min(image))
image=image.astype(np.float32)
print(np.max(image), np.min(image))
image[image>255]=255
image[image<0]=0
print(np.max(image), np.min(image))

os.makedirs('lab2/problem6',exist_ok=True)
for q in range(1,9):
    dst_path=os.path.join('lab2/problem6', f"boats_{q}.bmp")
    d=pow(2,8-q)
    print(d)
    image_copy=image.copy()

    noise=np.random.uniform(0,1,image_copy.shape)

    image_copy= (np.floor(image_copy/d + noise)+1/2)*d

    image_copy=image_copy.astype(np.uint8)
    cv2.imwrite(dst_path,image_copy)
