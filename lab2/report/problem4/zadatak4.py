import os
import cv2

src_dir_path='lab2/slike'
dst_dir_path='lab2/problem4'
os.makedirs(dst_dir_path, exist_ok=True)

for threshold in [63, 127, 191]:
    for image_name in os.listdir(src_dir_path):
        image_path=os.path.join(src_dir_path,image_name)
        image=cv2.imread(image_path, cv2.IMREAD_GRAYSCALE)
        image_threshold=  image * (image>threshold)
        image_threshold_dst_path=os.path.join(dst_dir_path,image_name.split('.')[0]+f"_{threshold}_thresh.jpg")
        print(image_threshold_dst_path)
        cv2.imwrite(image_threshold_dst_path,image_threshold)
