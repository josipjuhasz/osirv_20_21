import numpy as np
import cv2
import matplotlib.pyplot as plt

image=cv2.imread('C:/Users/jule/osirv_20_21/lab3/slike/baboon.bmp',0)


def salt_n_pepper_noise(image, percent=10):
  out = image.astype(np.float32)
  limit = ((float(percent)/2.0)/100.0) * 255.0
  noise = np.random.uniform(0,255, image.shape)
  out[noise<limit] = 0
  out[noise>(255-limit)] = 255
  out[out>255] = 255
  out[out<0] = 0
  return out.astype(np.uint8)

def uniform_noise(image, low, high):
  out = image.astype(np.float32)
  noise = np.random.uniform(low,high, image.shape)
  out = out + noise
  out[out>255] = 255
  out[out<0] = 0
  return out.astype(np.uint8)

def gaussian_noise(image, mu, sigma):
  out = image.astype(np.float32)
  noise = np.random.normal(mu, sigma, image.shape)
  out = out + noise
  out[out>255] = 255
  out[out<0] = 0
  return out.astype(np.uint8)


  
def showhist(image):
  hist,bins = np.histogram(image.flatten(), bins=256, range=(0,255))
  plt.vlines(np.arange(len(hist)), 0, hist)
  plt.title("Histogram")
  plt.show()
  

showhist(gaussian_noise(image, 0, 15))
showhist(gaussian_noise(image, 0, 25))
showhist(gaussian_noise(image, 0, 40))
showhist(salt_n_pepper_noise(image, 5))
showhist(salt_n_pepper_noise(image, 10))
showhist(salt_n_pepper_noise(image, 15))
showhist(salt_n_pepper_noise(image, 20))
showhist(uniform_noise(image, -20, 20))
showhist(uniform_noise(image, -40, 40))
showhist(uniform_noise(image, -60, 60))

cv2.imwrite("baboon_gaussian_noise_15.jpg", gaussian_noise(image, 0, 15))
cv2.imwrite("baboon_gaussian_noise_25.jpg", gaussian_noise(image, 0, 25))
cv2.imwrite("baboon_gaussian_noise_40.jpg", gaussian_noise(image, 0, 40))
cv2.imwrite("baboon_salt_n_pepper_noise_5.jpg", salt_n_pepper_noise(image, 5))
cv2.imwrite("baboon_salt_n_pepper_noise_10.jpg", salt_n_pepper_noise(image, 10))
cv2.imwrite("baboon_salt_n_pepper_noise_15.jpg", salt_n_pepper_noise(image, 15))
cv2.imwrite("baboon_salt_n_pepper_noise_20.jpg", salt_n_pepper_noise(image, 20))
cv2.imwrite("baboon_uniform_noise_20.jpg", uniform_noise(image, -20, 20))
cv2.imwrite("baboon_uniform_noise_40.jpg", uniform_noise(image, -40, 40))
cv2.imwrite("baboon_uniform_noise_60.jpg", uniform_noise(image, -60, 60))






