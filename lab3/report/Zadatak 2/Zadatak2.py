import numpy as np
import cv2
import matplotlib.pyplot as plt

image1=cv2.imread('C:/Users/jule/osirv_20_21/lab3/slike/airplane.bmp')
image2=cv2.imread('C:/Users/jule/osirv_20_21/lab3/slike/boats.bmp')

def salt_n_pepper_noise(image, percent):
  out = image.astype(np.float32)
  limit = ((float(percent)/2.0)/100.0) * 255.0
  noise = np.random.uniform(0,255, image.shape)
  out[noise<limit] = 0
  out[noise>(255-limit)] = 255
  out[out>255] = 255
  out[out<0] = 0
  return out.astype(np.uint8)

def gaussian_noise(image, mu, sigma):
  out = image.astype(np.float32)
  noise = np.random.normal(mu, sigma, image.shape)
  out = out + noise
  out[out<0] = 0
  out[out>255] = 255
  return out.astype(np.uint8)



salt =[1,10]
gaussian=[5, 15, 35]
median=[3, 5, 7]
blur=[1, 5, 9]

for x in gaussian:
    _b = gaussian_noise(image2, 0, x)
    image_a = gaussian_noise(image1, 0, x)
    for y in median:
        image_med_b= cv2.medianBlur(image_b, y)
        cv2.imwrite('boats_gaussian_' + str(x) + '_median_' + str(y) + '.bmp', image_med_b)
        image_med_a= cv2.medianBlur(image_a, y)
        cv2.imwrite('airplane_gaussian_' + str(x) + '_median_' + str(y) + '.bmp', image_med_a)
    for z in blur:
        image_blur_b = cv2.GaussianBlur(image_b, (z, z), x)
        cv2.imwrite('boats_gaussian_'+ str(x) + '_gaussian_blur_' + str(z) + '.bmp', image_blur_b)
        image_blur_a = cv2.GaussianBlur(image_a, (z, z), x)
        cv2.imwrite('airplane_gaussian_'+ str(x) + '_gaussian_blur_' + str(z) + '.bmp', image_blur_a)

for x in salt:
    image_a = salt_n_pepper_noise(image1, x)
    image_b = salt_n_pepper_noise(image2, x)
    for y in median:
        image_med_a= cv2.medianBlur(image_a, y)
        cv2.imwrite('boats_salt_n_pepper_' + str(x) + '_median_' + str(y) + '.bmp', image_med_b)
        image_med_b= cv2.medianBlur(image_b, y)
        cv2.imwrite('airplane_salt_n_pepper_' + str(x) + '_median_' + str(y) + '.bmp', image_med_a)
    for z in blur:
        image_blur_b = cv2.GaussianBlur(image_b, (z, z), x)
        cv2.imwrite('boats_salt_n_papper_'+ str(x) + '_gaussian_blur_' + str(z) + '.bmp', image_blur_b)
        image_blur_a = cv2.GaussianBlur(image_a, (z, z), x)
        cv2.imwrite('airplane_salt_n_papper_'+ str(x) + '_gaussian_blur_' + str(z) + '.bmp', image_blur_a)

def med_filter(image, radius):
    imagecopy=image
    for i in range(image.shape[0]):
        for j in range(image.shape[1]):
            imagecopy[i][j]=np.median(imagecopy[i:i+radius, j:j+radius])
    return imagecopy
   
image_z4=cv2.imread('C:/Users/jule/osirv_20_21/lab3/slike/boats.bmp',0)

noise = salt_n_pepper_noise(image_z4, 10)
med1 = med_filter(image_z4, 3)
med2 = cv2.medianBlur(image_z4, 3)

cv2.imwrite('my_filter.jpg', med1)
cv2.imwrite('original_filter.jpg', med2)
cv2.imwrite('noise_test.jpg', noise)
