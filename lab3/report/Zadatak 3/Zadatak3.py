import numpy as np
import cv2
from matplotlib import pyplot as plt

airplane = cv2.imread('C:/Users/jule/osirv_20_21/lab3/slike/airplane.bmp', 0)
baboon = cv2.imread('C:/Users/jule/osirv_20_21/lab3/slike/baboon.bmp', 0)
boats = cv2.imread('C:/Users/jule/osirv_20_21/lab3/slike/boats.bmp', 0)
images = [baboon, airplane, boats]
image_names = ['baboon','airplane', 'boats']

for i in range(len(images)):
    ret, thresh1 = cv2.threshold(images[i], 127, 255, cv2.THRESH_BINARY)
    cv2.imwrite(image_names[i] + '_thresh_binary' + '.bmp', thresh1)
    ret, thresh2 = cv2.threshold(images[i], 127, 255, cv2.THRESH_BINARY_INV)
    cv2.imwrite(image_names[i] + '_thresh_binary_inv' + '.bmp', thresh2)
    ret, thresh3 = cv2.threshold(images[i], 127, 255, cv2.THRESH_TRUNC)
    cv2.imwrite(image_names[i] + '_thresh_trunc' + '.bmp', thresh3)
    ret, thresh4 = cv2.threshold(images[i], 127, 255, cv2.THRESH_TOZERO)
    cv2.imwrite(image_names[i] + '_thresh_tozero' + '.bmp', thresh4)
    ret, thresh5 = cv2.threshold(images[i], 127, 255, cv2.THRESH_TOZERO_INV)
    cv2.imwrite(image_names[i] + '_thresh_tozero_inv' + '.bmp', thresh5)
    th6 = cv2.adaptiveThreshold(images[i], 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY,11,2)
    cv2.imwrite(image_names[i] + '_thresh_adaptive_mean' + '.bmp', th6)
    th7 = cv2.adaptiveThreshold(images[i], 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY,11,2)
    cv2.imwrite(image_names[i] + '_thresh_adaptive_gaussian' + '.bmp', th7)
    ret, th8 = cv2.threshold(images[i], 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
    cv2.imwrite(image_names[i] + '_thresh_otsu' + '.bmp', th8)
    
