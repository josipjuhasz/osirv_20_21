import numpy as np
import cv2

img = cv2.imread('../slike/baboon.bmp')

blue = img.copy()
blue[:,:,(1,2)]*=0

green = img.copy()
green[:,:,(0,2)]*=0

red = img.copy()
red[:,:,(0,1)]*=0

cv2.imwrite("plava.jpg", blue)
cv2.imwrite("zelena.jpg", green)
cv2.imwrite("crvena.jpg", red)
