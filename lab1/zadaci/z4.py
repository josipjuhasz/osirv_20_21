import numpy as np
import cv2

img = cv2.imread('../slike/baboon.bmp')

first = img.copy()
first = first[:, ::2, :]

second = img.copy()
second = second[::2, :, :]

third = img.copy()
third = second[::2, ::2, :]

cv2.imshow("first", first)
cv2.imshow("second", second)
cv2.imshow("third", third)

cv2.waitKey(0)
cv2.destroyAllWindows()
