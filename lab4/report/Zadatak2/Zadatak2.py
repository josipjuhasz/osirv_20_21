import numpy as np
import argparse
import glob
import cv2
from matplotlib import pyplot as plt

def auto_canny(image, sigma=0.33):
        # compute the median of the single channel pixel intensities
        v = np.median(image)

        # apply automatic Canny edge detection using the computed median
        lower = int(max(255, (1.0 - sigma) * v))
        upper = int(min(255, (1.0 + sigma) * v))
        edged = cv2.Canny(image, lower, upper)
        print ("Median lower: %r." % lower)
        print ("Median upper: %r." % upper)
        # return the edged image
        return edged

image = cv2.imread(r"C:/Users/jule/Desktop/ObradaLabos/osirv_20_21/lab4/slike/pepper.bmp", 0)
auto = auto_canny(image)

plt.subplot(121),plt.imshow(image, cmap='gray')
plt.title('Original Image'), plt.xticks([]), plt.yticks([])
plt.subplot(122),plt.imshow(auto,  'gray')
plt.title('Edges'), plt.xticks([]), plt.yticks([])
plt.show()
